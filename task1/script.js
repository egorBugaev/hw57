const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];
const workType ={
    Frontend:0,
    Backend:0
};
const workMoreThen4Hours = [];
let totalFrontEndTime = 0;
let totalBackEndTime = 0;
let totalBugTime = 0;
let totalUItasks = 0;
const Frontend = () => {
   tasks.map(function (task) {
       if (task.category === "Backend") {
           workType.Backend++;
           totalBackEndTime += task.timeSpent;
       } else {
           workType.Frontend++;
           totalFrontEndTime += task.timeSpent;
       }
    if(task.type ==="bug"){
        totalBugTime+= task.timeSpent;
    }
    if(task.title.includes("UI")){
        totalUItasks ++;
    }
    if(task.timeSpent > 4){
           let test ={title: task.title, category: task.category};
           workMoreThen4Hours.push(test);
    }
   })
};
Frontend();
console.log(workType);
console.log("Общее количество времени, затраченное на работу над задачами из категории 'Backend': " + totalBackEndTime);
console.log("Общее количество времени, затраченное на работу над задачами из категории 'Frontend': " + totalFrontEndTime);
console.log("Общее количество времени, затраченное на работу над задачами типа 'bug': " + totalBugTime);
console.log("Количество задач, имеющих в названии слово 'UI': " + totalUItasks);
console.log(/*"Массив задач с затраченным временем больше 4 часов : " +*/ workMoreThen4Hours);

