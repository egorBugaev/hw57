import React, {Component} from 'react'
import './Wallet.css';
import Top from "./components/Top/Top";
import Content from "./components/content/content";
import Diagram from "./components/content/diagram/diagram";

class Wallet extends Component {
	
	state = {
		costs: [],
		name: 'Item name',
		cost: '0',
		category: 'Other',
		totalSum: 0
	};
	
	changeName = (event) => {
		let name = this.state.name;
		name = event.target.value;
		this.setState({name});
	};
	
	changeCost = (event) => {
		let cost = this.state.cost;
		cost = event.target.value.replace(/[^0-9]/g,'');
		this.setState({cost});
	};
	
	addItem = () => {
		const costs = [...this.state.costs];
		let totalSum = this.state.totalSum;
		let name = this.state.name;
		let cost = this.state.cost;
		let category = this.state.category;
		let randomID = name + cost + Math.random();
		let newCost = {category: category,name: name, price: cost, id: randomID};
		costs.push(newCost);
		totalSum += parseInt(cost, 0);
		this.setState({costs, totalSum});

	};
	
	deleteItem = (id) => {
		const costs = [...this.state.costs];
		const index = costs.findIndex(p => p.id === id);
		let totalSum = this.state.totalSum;
		totalSum -= costs[index].price;
		costs.splice(index, 1);
		this.setState({costs, totalSum});
	};
	
	resetName = () => {
		let name = this.state.name;
		name = '';
		this.setState({name});
	};
	
	resetCost = () => {
		let cost = this.state.cost;
		cost = '';
		this.setState({cost});
	};
	
	changeCategory = (event) => {
		let category = this.state.category;
		category = event.target.value;
		this.setState({category});
	};
	
	render() {
		return (
			<div className="Wallet">
				<Top
					changeName={(event) => this.changeName(event)}
					changeCost={(event) => this.changeCost(event)}
					addItem={() => this.addItem()}
					name={this.state.name}
					cost={this.state.cost}
					resetName={(event) => this.resetName(event)}
					resetCost={(event) => this.resetCost(event)}
					changeCategory={(event) => this.changeCategory(event)}
				/>
				<Content
					costs={this.state.costs}
					totalSum={this.state.totalSum}
                    deleteItem={this.deleteItem}
				/>
				<Diagram costs={this.state.costs}/>
			</div>
		)
	}
	
}

export default Wallet;