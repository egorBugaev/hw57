import React from 'react';

const Cost = props => {
	return (
		<div className="list">
			<span className="category">{props.category}</span>
			<span className="name">{props.name}</span>
			<span className="price">{props.price} KGS</span>
			<button onClick={() => props.deleteItem(props.id)} className="delete">
				Delete
			</button>
		</div>
	)
};

export default Cost;