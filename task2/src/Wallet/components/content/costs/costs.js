import React from 'react';
import Cost from "./cost/cost";

const Costs = props => {
	return (
		<div>
			{
				props.costs.map(cost => {
					return <Cost
						key={cost.id}
						name={cost.name}
						price={cost.price}
                        deleteItem={props.deleteItem}
						id={cost.id}
						category={cost.category}
					/>;
				})
			}
		</div>
	)
};

export default Costs;