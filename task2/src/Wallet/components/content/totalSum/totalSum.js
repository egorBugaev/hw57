import React from 'react';

const TotalSum = props => {
	return (
		<div className="totalSum">Total sum: <span>{props.totalSum} KGS</span></div>
	)
};

export default TotalSum;