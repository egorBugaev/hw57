import React from 'react';

const Diagram = props => {
	const categoryInfo = props.costs.reduce((acc, cost) => {
		switch(cost.category) {
			case 'Other':
				acc.other += parseInt(cost.price,0);
				break;
			case 'Entertainment':
				acc.entertainment += parseInt(cost.price,0);
				break;
			case 'Car':
				acc.car += parseInt(cost.price,0);
				break;
			case 'Food':
				acc.food += parseInt(cost.price,0);
				break;
			default: return null;
		}
		return acc;
	}, {other: 0, entertainment: 0, car: 0, food: 0});
	let totalSum = Object.values(categoryInfo).reduce((acc,price) => acc += price);

	let elementsStyle = {
		other: {width: 0},
		entertainment: {width: 0},
		car: {width: 0},
		food: {width: 0}
	};
    Object.keys(categoryInfo).map(key =>  {
        if(totalSum !== 0) {
        	elementsStyle[key].width = (categoryInfo[key] / totalSum *100)+'%';
        }
    });

	return (
		<div className="Diagram">
			<span style={elementsStyle.other} className="other"/>
			<span style={elementsStyle.entertainment} className="entertainment"/>
			<span style={elementsStyle.car} className="car"/>
			<span style={elementsStyle.food} className="food"/>
		</div>
	)
};

export default Diagram;