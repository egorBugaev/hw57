import React from 'react'
import Costs from "./costs/costs";
import TotalSum from "./totalSum/totalSum";

const Content = props => {
	return (
		<div className="content">
			<Costs costs={props.costs} deleteItem={props.deleteItem}/>
			<TotalSum totalSum={props.totalSum}/>
		</div>
	)
};

export default Content;