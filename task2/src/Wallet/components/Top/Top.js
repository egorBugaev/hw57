import React from 'react';

const Top = props => {
	return (
		<div className="top">
			<select defaultValue="Other" onChange={props.changeCategory}  className="select">
				<option value="Other">Other</option>
				<option value="Entertainment">Entertainment</option>
				<option value="Car">Car</option>
				<option value="Food">Food</option>
			</select>
			<input onClick={props.resetName} onChange={props.changeName} className="name" type="text" value={props.name}/>
			<input onClick={props.resetCost} onChange={props.changeCost} className="cost" type="text" value={props.cost}/>
			<span className="currency">KGS</span>
			<button onClick={props.addItem} className="add">Add</button>
		</div>
	)
};

export default Top;